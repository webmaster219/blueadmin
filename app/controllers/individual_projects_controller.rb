class IndividualProjectsController < ApplicationController
  before_filter :authenticate_user!
  require 'money/bank/google_currency'
  def new
    @project = Project.new
  end

  def create
    Money.default_bank = Money::Bank::GoogleCurrency.new
    
    @project = current_user.projects.build(params[:project].permit(:site_code, :name, :domain, :type, :monetisation, :description, :monthly_target, :attach))
    @project.amount_money     = Money.new(params[:project][:amount_money].to_f * 100.0, current_user.currency_type) 
    @project.converted_money  = @project.amount_money.exchange_to(current_user.currency_type.to_sym)

    @project.live_date = Time.now
    @project.active = true
    respond_to do |format|
      if @project.save
        format.html { redirect_to action: :index}
        format.json { render json: @project, status: :created, location: @project}
        flash[:notice] = 'Project was created successfully'
      else
        format.html { render action: :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def index
    limit = params[:page_limit].present? ? params[:page_limit].to_i : 10
    @projects = current_user.live_projects.all.paginate(page: params[:page], :per_page => limit)
    if params[:fid].present?
      ids = params[:fid].chop!
      afs = AttachFile.in(id:ids.split(","))
      afs.destroy_all
    end
  end

  def edit
    @year = params[:year].present? ? params[:year].to_i : Date.today.year
    @project = Project.find(params[:id])
    @time_spent = @project.time_spents.build
    @monthly_order = @project.monthly_orders.build
    @monthly_income = @project.monthly_incomes.build

    @accounts = @project.accounts.paginate(page: params[:account_page], :per_page => 10)
    @articles = @project.articles.paginate(page: params[:article_page], :per_page => 10)
    @keywords = @project.keywords.paginate(page: params[:keyword_page], :per_page => 10)
    @tasks = @project.tasks.paginate(page: params[:task_page], :per_page => 10)
    @spents = @project.time_spents.paginate(page: params[:spent_page], :per_page => 10)
    
    order_values = []
    income_values = []
    [*1..12].each do |m|
      order = @project.monthly_orders.where(year:@year, month:m).first
      income = @project.monthly_incomes.where(year:@year, month:m).first
      order_values[m-1] = order.present? ? order.orders : 0
      income_values[m-1] = income.present? ? income.incomes : 0
    end
    
    @chart1 = LazyHighCharts::HighChart.new('basic_line') do |f|
      f.title(:text => "Orders V Incomes")
      f.xAxis(:categories => Date::MONTHNAMES.reject{|m| m.nil?}.map{|m| m[0..2]})
      f.series(:name => "Orders", :yAxis => 0, :data => order_values) if @project.has_orders?
      f.series(:name => "Income", :yAxis => 0, :data => income_values)

      f.legend(:align => 'right', :verticalAlign => 'top', :y => 0, :x => 10, :layout => 'vertical', width: 0,)
      f.chart({type: 'line', marginRight: 15, marginLeft: 50,marginBottom: 30, marginTop: 60, :defaultSeriesType=>"column" })
    end

    time_spents = []
    [*1..12].each do |m|
      time_spent = @project.time_spents.where(year:@year, month:m).first      
      time_spents[m-1] = time_spent.present? ? time_spent.time : 0
    end
    
    @chart2 = LazyHighCharts::HighChart.new('basic_line') do |f|
      f.title(:text => "Timespent")
      f.xAxis(:categories =>  Date::MONTHNAMES.reject{|m| m.nil?}.map{|m| m[0..2]})
      f.series(:name => "Timespent", :yAxis => 0, :data => time_spents)

      f.legend(:align => 'right', :verticalAlign => 'top', :y => 0, :x => 10, :layout => 'vertical', width: 0,)
      f.chart({type: 'line', marginRight: 15, marginLeft: 50,marginBottom: 30, marginTop: 60, :defaultSeriesType=>"column" })
    end
  end

  def update    
    @project = Project.find(params[:id])
    #respond_to |form|
      case params[:object]
      when "0"
        time_spent = @project.time_spents.where(year:params[:time_spent][:year], month:params[:time_spent][:month]).first
        if time_spent.present?
          time_spent.assign_attributes(params[:time_spent].permit(:title, :year, :month, :time, :description, :owner))
        else
          time_spent = @project.time_spents.build(params[:time_spent].permit(:title, :year, :month, :time, :description, :owner))
        end  
        
        if time_spent.save
          data = {success: "Added Time Spent"}
        else
          data = {failure: "Please check try again"}
        end
      when "1"
        monthly_order = @project.monthly_orders.where(year:params[:order][:year], month:params[:order][:month]).first
        if monthly_order.present?
          monthly_order.assign_attributes(orders:params[:order][:order])
        else
          monthly_order = @project.monthly_orders.build(year:params[:order][:year], month:params[:order][:month], orders:params[:order][:order])
        end
        if monthly_order.save
          data = {success: "Added Monthly Order"}
        else
          data = {failure: monthly_order.errors.messages.first}
        end
      when "2"
        monthly_income = @project.monthly_incomes.where(year:params[:income][:year],month:params[:income][:month]).first
        if monthly_income.present?
          monthly_income.assign_attributes(incomes:params[:income][:income])
        else
          monthly_income = @project.monthly_incomes.build(year:params[:income][:year], month:params[:income][:month], incomes:params[:income][:income])
        end
        if monthly_income.save
          data = {success: "Added Monthly Income"}
        else
          data = {failure: "Please check try again"}
        end
      when "4"
        project_id = params[:prj_id]
        year = params[:bulk_data][:year].to_i

        income = params[:bulk_income]
        Date::MONTHNAMES.reject{|m| m.nil?}.each_with_index do |m, i|
          if params[:bulk_orders].present?
            order = MonthlyOrder.where(project_id:project_id, year:year, month:i+1).first
            if order.present?
              order.update_attributes(orders:params[:bulk_orders][m.downcase.to_sym])
            else
              MonthlyOrder.create(project_id:project_id, year:year, month: i+1, orders:params[:bulk_orders][m.downcase.to_sym])
            end
          end

          if params[:bulk_income].present?            
            income = MonthlyIncome.where(project_id:project_id, year:year, month:i+1).first
            if income.present?
              income.update_attributes(incomes:params[:bulk_income][m.downcase.to_sym])
            else
              MonthlyIncome.create(project_id:project_id, year:year, month: i+1, incomes:params[:bulk_income][m.downcase.to_sym])
            end
          end
        end
        data = {success: "Added Bulk Data"}
      end
    #end
    render json: data
  end

  def destroy
    @project = Project.find(params[:id])
    @project.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
      format.json { head :no_content }
      flash[:notice] = "Project was deleted"
    end
  end
    
  def get_year_data
    @project = Project.find(params[:prj_id])
    @year = params[:year]
    render layout: false
  end

  def get_year_bulk_data
    @project = Project.find(params[:prj_id])
    @year = params[:year]
    render layout: false
  end
end
