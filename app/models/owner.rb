class Owner
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,                          type: String
  field :email,                         type: String

  belongs_to :user
  has_many :tasks

  validates_presence_of :user_id, :name, :email
  # validates :email, :uniqueness => {:scope => :user_id}  
  validates_uniqueness_of :email, scope: :user_id
end
