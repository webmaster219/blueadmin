class TimeSpent
  include Mongoid::Document
  include Mongoid::Timestamps

  # mount_uploader :attach, TaskAttachUploader

  field :title,                 type: String  
  field :time,                  type: Integer
  field :description,           type: String
  field :year,                  type: Integer
  field :month,                 type: Integer 
  # field :attach,              type: String

  belongs_to :owner
  belongs_to :project
end
