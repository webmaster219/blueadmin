class Admin::UsersController < ApplicationController    
  before_filter :authenticate_admin
  
  def index    
    @users = User.all.paginate(page: params[:page], :per_page => 15)
  end


  def send_to_reset_password_email
    @user = User.find(params[:id])
    UserMailer.reset_password(@user).deliver
    flash[:notice] = "Sent the reset password email successfully"
    redirect_to action: :index
  end

  def setting    
  end

  def new_item
    type = params[:new_object]
    item = params[:item]
    case type
    when '0'
      option = Option.create_by_option_name(:project_types)
      opt_val = option.option_value.split(",")
      opt_val << item
      option.update_attribute(:option_value, opt_val.join(","))
    when '1'
      option = Option.create_by_option_name(:moneitions)
      opt_val = option.option_value.split(",")
      opt_val << item
      option.update_attribute(:option_value, opt_val.join(","))
    when '2'
      option = Option.create_by_option_name(:expense_items)
      opt_val = option.option_value.split(",")
      opt_val << item
      option.update_attribute(:option_value, opt_val.join(","))
    when '3'
      option = Option.create_by_option_name(:expense_payments)
      opt_val = option.option_value.split(",")
      opt_val << item
      option.update_attribute(:option_value, opt_val.join(","))
    end
    redirect_to action: :setting
  end
  def update_setting_option
    type = params[:update_object]
    item_number = params[:item_id].to_i
    item = params[:item]
    case type
    when '0'
      option = Option.create_by_option_name(:project_types)
      opt_val = option.option_value.split(",")
      opt_val[item_number] = item
      option.update_attribute(:option_value, opt_val.join(","))
    when '1'
      option = Option.create_by_option_name(:moneitions)
      opt_val = option.option_value.split(",")
      opt_val[item_number] = item
      option.update_attribute(:option_value, opt_val.join(","))
    when '2'
      option = Option.create_by_option_name(:expense_items)
      opt_val = option.option_value.split(",")
      opt_val[item_number] = item
      option.update_attribute(:option_value, opt_val.join(","))
    when '3'
      option = Option.create_by_option_name(:expense_payments)
      opt_val = option.option_value.split(",")
      opt_val[item_number] = item
      option.update_attribute(:option_value, opt_val.join(","))
    end
    redirect_to action: :setting
  end

  def delete_items
    type = params[:object]
    item_number = params[:id].to_i

    case type
    when '0'
      option = Option.create_by_option_name(:project_types)
      opt_val = option.option_value.split(",")
      opt_val.delete_at(item_number)
      option.update_attribute(:option_value, opt_val.join(","))
    when '1'
      option = Option.create_by_option_name(:moneitions)
      opt_val = option.option_value.split(",")
      opt_val.delete_at(item_number)
      option.update_attribute(:option_value, opt_val.join(","))
    when '2'
      option = Option.create_by_option_name(:expense_items)
      opt_val = option.option_value.split(",")
      opt_val.delete_at(item_number)
      option.update_attribute(:option_value, opt_val.join(","))
    when '3'
      option = Option.create_by_option_name(:expense_payments)
      opt_val = option.option_value.split(",")
      opt_val.delete_at(item_number)
      option.update_attribute(:option_value, opt_val.join(","))
    end
    redirect_to action: :setting
  end

end
