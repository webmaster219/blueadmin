class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # include SimpleCaptcha::ControllerHelpers

  def authenticate_admin             # property manager
    if !current_user.present?
      redirect_to new_user_session_path
    elsif current_user.present? && (current_user.role == "admin")
      # proceed as normal
    else
      flash[:error] = "Access is for property manager only"
      redirect_to root_path
    end
  end

end
