class InvoiceMailer < ActionMailer::Base
  include Stripe::Callbacks

  after_invoice_created! do |invoice, event|
    user = User.find_by_stripe_customer(invoice.customer)
    new_invoice(user, invoice).deliver
  end

  def new_invoice(user, invoice)
    @user = user
    @invoice = invoice
    mail :to => user.email, :subject => '[Acme.com] Your new invoice'
  end
end