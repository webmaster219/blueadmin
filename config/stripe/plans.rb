# This file contains descriptions of all your stripe plans

# Example
# Stripe::Plans::PRIMO #=> 'primo'

# Stripe.plan :primo do |plan|
#
#   # plan name as it will appear on credit card statements
#   plan.name = 'Acme as a service PRIMO'
#
#   # amount in cents. This is 6.99
#   plan.amount = 699
#
#   # interval must be either 'week', 'month' or 'year'
#   plan.interval = 'month'
#
#   # only bill once every three months (default 1)
#   plan.interval_count = 3
#
#   # number of days before charging customer's card (default 0)
#   plan.trial_period_days = 30
# end

# Once you have your plans defined, you can run
#
#   rake stripe:prepare
#
# This will export any new plans to stripe.com so that you can
# begin using them in your API calls.

Stripe.plan :large do |plan|
  plan.name = 'FOR LARGE TEAMS'
  plan.amount = 1999 # $19.99
  plan.interval = 'month'
end

Stripe.plan :medium do |plan|
  plan.name = 'FOR MEDIUM TEAMS'
  plan.amount = 2999 # $29.99
  plan.interval = 'month'
end

Stripe.plan :most do |plan|
  plan.name = 'PERFECT FOR MOST'
  plan.amount = 4900 # $49.00
  plan.interval = 'month'
end

Stripe.plan :person do |plan|
  plan.name = 'FOR ONE PERSON'
  plan.amount = 7900 # $79.00
  plan.interval = 'month'
end