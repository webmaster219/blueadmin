class Image
  include Mongoid::Document
  include Mongoid::Timestamps
  mount_uploader :file, TinymceImageUploader

  field :file,            type: String
  field :alt,             type: String
  field :hint,            type: String

  belongs_to :user
  

  def img_url
    if self.file.url.nil?
      ""
    else
      # if Rails.env.production?
        self.file.url
      # else
      #   "http://localhost:3000" + self.file.url.gsub("#{Rails.root.to_s}/public/", "/")        
      # end
    end
  end
end
