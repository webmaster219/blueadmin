class Account
  include Mongoid::Document
  include Mongoid::Timestamps

  TYPES = %w[Facebook Twitter Linkedin Instagram Gmail HotMail Other]

  field :type,                  type: Integer,        default: 6
  field :user_name,             type: String
  field :password,              type: String
  field :url,                   type: String

  validates_presence_of :type, :user_name, :url, :project_id

  belongs_to :project

  def type_name
    Account::TYPES[type] if type.present?
  end

  def encrypted_password
    if password.length > 0
      pswd = []
      pswd[password.length] = "*"
      pswd.fill("*").join()
    else
      ""
    end
  end

end
