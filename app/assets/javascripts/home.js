// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function(){
    $('.input-group.date').datepicker({
      format: "dd/mm/yyyy"
    });
    
    // tinyMCE.init({
    //   theme : "modern",
    //   mode : "textareas",
    //   plugins: "image",
    //   selector: 'textarea.tinymce'
    // })

    $("textarea.tinymce").tinymce({
      theme: "modern",
      toolbar: "bold,italic,underline,|,bullist,numlist,outdent,indent,|,undo,redo,|,pastetext,pasteword,selectall,|,uploadimage",
      pagebreak_separator: "<p class='page-separator'>&nbsp;</p>",
      plugins: ["uploadimage"],
      relative_urls: false,
      remove_script_host: false,
      document_base_url: (!window.location.origin ? window.location.protocol + "//" + window.location.host : window.location.origin) + "/",
    })
  })

