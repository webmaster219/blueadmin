class Article
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,                type: String

  field :pri_key,             type: String
  field :sec_key,             type: String

  field :lsi,                 type: String

  validates_presence_of :project_id, :name

  belongs_to :project
end
