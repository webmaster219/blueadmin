class Expense
  include Mongoid::Document
  include Mongoid::Timestamps

  ITEM_TYPE = %w[Product Affliate\ Payments Advertising Aweber Bank/Finance Charges Broadband Bookeeping Business\ Entertainment Computer\ Hardware Copywriting Content Domains Exhibitions/Conferences Hosting Graphics Insurance Legal Licenising Motor\ Expenses Mechant\ Fees Office\ Rental Office\ Equipment Staff\ Training Plugins SaaS Software SocialAdr Staffing Subscriptions Recurring\ Payments Sundries Training Themes Telephone Travel Wage Pay SEM SEO CPM CPE CPC CPA CPV CPL PPC VA GA Website]
  BUDGET_TYPE = %w[BBY SMR SCR]
  PAYMENT_METHODS = %w[Cheque Personal\ Paypal\ Acc Business\ Paypal\ Acc Amazon\ Pay Google\ Checkout 2Checkout Business\ Credit\ Card Personal\ Debit Card Personal\ Credit Card Skrill Other]

  field :expense_id,          type: String  
  field :item_type,           type: Integer

  field :title,               type: String
  field :date,                type: DateTime, default:Time.now

  
  field :payment_method,      type: Integer
  field :budget_pot,          type: Integer
  
  #field :amount,             type: Float,      default: 0
  field :amount_money,        type: Money
  field :converted_money,     type: Money

  field :description,         type: String
  field :recurring,           type: Boolean,      default: false
  field :original,            type: Boolean,      default: false
  field :attach_file_ids,     type: String

  attr_accessor :currency_type


  belongs_to :project
  validates_presence_of :project_id, :title, :amount_money

  scope :recurring_expenses, -> {where(recurring:true, original:true)}

  def attach_files
    if attach_file_ids.present?
      ids = attach_file_ids.split(",")
      AttachFile.in(id:ids)
    else
      []
    end
  end
  def amount
    converted_money.present? ? converted_money.fractional/100.0 : 0
  end
  def currency_symbol
    money = Money.new(1,project.user.currency_type)
    converted_money.present? ? converted_money.currency.symbol : money.currency.symbol
  end

  def site_code_name
    project.site_code
  end
  
  def item_type_name
    Expense::item_types[item_type] if item_type.present?
  end

  def budget_type_name
    Expense::BUDGET_TYPE[budget_pot] if budget_pot.present?
  end

  def payment_method_name
    Expense::payment_methods[payment_method] if payment_method.present?
  end

  def self.most_budget_pot
    return "" if Expense.count == 0
    map = %Q{
      function(){
        emit(this.payment_method, {count: 1})
      }
    }
    reduce = %Q{
      function(key, values){
        var result = {count:0};
        values.forEach(function(value){
          result.count += value.count;
        });
        return result;
      }
    }
    results = Expense.map_reduce(map,reduce).out(inline: true).to_a
    item = results.first
    results.each do |r|
      if item["value"]["count"].to_i < r["value"]["count"].to_i
        item = r
      end
    end
    Expense::payment_methods[item["_id"].to_i]
  end


  def self.most_spent_on
    return "" if Expense.count == 0
    map = %Q{
      function(){
        emit(this.item_type, {count: 1})
      }
    }
    reduce = %Q{
      function(key, values){
        var result = {count:0};
        values.forEach(function(value){
          result.count += value.count;
        });
        return result;
      }
    }
    results = Expense.map_reduce(map,reduce).out(inline: true).to_a
    item = results.first
    results.each do |r|
      if item["value"]["count"].to_i < r["value"]["count"].to_i
        item = r
      end
    end
    Expense::item_types[item["_id"].to_i]
  end
  
  def self.add_expense_data
    cur_date = Time.now.to_date
    Expense.recurring_expenses.each do |expense|
      count = (cur_date - expense.date).to_i
      if count % 30 == 0        
        exp = Expense.create(project_id:expense.project_id,
                          item_type:expense.item_type, 
                          title:expense.title, 
                          date:Time.now, 
                          payment_method:expense.payment_method, 
                          budget_pot:expense.budget_pot,
                          amount_money:expense.amount_money, 
                          converted_money:expense.converted_money,
                          attach_file_ids:expense.attach_file_ids, 
                          description:expense.description)        

      end
    end
  end

  def self.top_three_site_code
    return "" if Expense.count == 0
    map = %Q{
      function(){
        emit(this.project_id, {count: 1})
      }
    }
    reduce = %Q{
      function(key, values){
        var result = {count:0};
        values.forEach(function(value){
          result.count += value.count;
        });
        return result;
      }
    }
    results = Expense.map_reduce(map,reduce).out(inline: true).to_a
    spent = results.first
    results.each do |r|
      if spent["value"]["count"].to_i < r["value"]["count"].to_i
        spent = r
      end
    end
    Project.find(spent["_id"]).site_code
  end

  def self.amount_of_month(year, month)
    expenses = self.where("return this.date.getFullYear() == #{year.to_i} && this.date.getMonth() == #{month.to_i-1}")
    if expenses.count > 0
      exps = expenses.map{|ex| ex.amount}
      exps.sum.round(2)
    else
      0
    end
  end
  def self.amount_of_year year
    expenses = self.where("return this.date.getFullYear() == #{year.to_i}")
    if expenses.count > 0
      expenses.map(&:amount).sum
    else
      0
    end
    
  end

  def self.amount_of_all
    self.all.map(&:amount).sum.round(2)
  end

  def self.item_types
    option = Option.create_by_option_name(:expense_items)
    if option.option_value.present?
      option.option_value.split(",")
    else
      []
    end
  end

  def self.payment_methods
    option = Option.create_by_option_name(:expense_payments)
    if option.option_value.present?
      option.option_value.split(",")
    else
      []
    end
  end
end
