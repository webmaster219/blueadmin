class InitProjectsController < ApplicationController
  before_filter :authenticate_user!
  require 'csv'
  def index
    @init_projects = current_user.init_projects.paginate(page: params[:page], :per_page => 10)
  end

  def new
    @project = InitProject.new
  end
  
  def create
    # @init_project = current_user.init_projects.build(params[:init_project].permit(:site_code, :year, :month, :budget, :income, :order))
    # respond_to do |format|
    #   if @init_project.save
    #     format.html { redirect_to action: :index}
    #     format.json { render json: @init_project, status: :created, location: @init_project}
    #     flash[:notice] = 'Created successfully'
    #   else
    #     format.html { render action: :new }
    #     format.json { render json: @init_project.errors, status: :unprocessable_entity }
    #   end      
    # end
  end

  def edit
    @project = InitProject.find(params[:id])
  end

  def update
    # init_project = InitProject.find(params[:id])    
    # respond_to do |format|
    #   if init_project.update_attributes(params[:init_project].permit(:site_code, :year, :month, :budget, :income, :order))
    #     format.html { redirect_to action: :index}
    #     format.json { render json: @init_project, status: :created, location: @init_project}
    #     flash[:notice] = 'Updated'
    #   else
    #     format.html { render action: :new }
    #     format.json { render json: @init_project.errors, status: :unprocessable_entity }
    #   end      
    # end
  end

  def destroy
    @project = InitProject.find(params[:id])
    redirect_to action: :index if @project.destroy
  end

  def import_csv
    year = params[:init_project][:year].to_i
    csv_file = params[:init_project][:csv_file]
    table = params[:init_project][:table]
    missed_row = []
    if csv_file.nil?
      flash[:error] = "Please select csv file and table name"
      redirect_to action: :index and return
    end

    case table
    when InitProject::IMPORT_TABLES[0]          # accounts
      headers = CSV.read(csv_file.path, headers: true).headers
      if headers.count != 5
        flash[:error] = "Can't import accounts data. Please check the csv file format"
        redirect_to action: :index and return
      end
      CSV.foreach(csv_file.path, headers:true) do |row|
        columns = row.to_a        
        project = Project.where({:site_code=>/^#{columns[0][1]}$/i}).first
        if project.present?
          account = Account.where(project_id:project.id, type:Account::TYPES.index(columns[1][1])).first
          if account.present?
            account.update_attributes(user_name:columns[2][1], password:columns[3][1], url:columns[4][1])
          else
            Account.create(project_id:project.id, type:Account::TYPES.index(columns[1][1]), user_name:columns[2][1], password:columns[3][1], url:columns[4][1])
          end
        else
          missed_row << columns[0][1]
        end
      end
    when InitProject::IMPORT_TABLES[1]          # articles
      headers = CSV.read(csv_file.path, headers: true).headers
      if headers.count != 5
        flash[:error] = "Can't import articles data. Please check the csv file format"
        redirect_to action: :index and return
      end

      CSV.foreach(csv_file.path, headers:true) do |row|
        columns = row.to_a
        project = Project.where({:site_code=>/^#{columns[0][1]}$/i}).first
        if project.present?
          article = Article.where(project_id:project.id, name:columns[1][1]).first
          if article.present?
            article.update_attributes(pri_key:columns[2][1], sec_key:columns[3][1], lsi:columns[4][1])
          else
            Article.create(project_id:project.id, name:columns[1][1], pri_key:columns[2][1], sec_key:columns[3][1], lsi:columns[4][1])
          end
        else
          missed_row << columns[0][1]
        end        
      end
    when InitProject::IMPORT_TABLES[2]          # future projects
      headers = CSV.read(csv_file.path, headers: true).headers
      if headers.count != 7
        flash[:error] = "Can't import future projects data. Please check the csv file format"
        redirect_to action: :index and return
      end
      CSV.foreach(csv_file.path, headers:true) do |row|
        columns = row.to_a        
        if columns[0][1].delete(" ").length == 3
          project = Project.where({:site_code=>/^#{columns[0][1]}$/i}).first
          st_dt = Date.strptime(columns[5][1],"%d/%m/%Y")
          ed_dt = Date.strptime(columns[6][1],"%d/%m/%Y")
          money = Money.new(columns[4][1].to_f * 100.0, current_user.currency_type) 
          if project.nil?
            project = current_user.projects.create(site_code:columns[0][1], name: columns[1][1], type:columns[2][1], monetisation:columns[3][1], amount_money:money, converted_money:money, start_date:st_dt, end_date:ed_dt)
          else
            project.update_attributes(name: columns[1][1], type:columns[2][1], monetisation:columns[3][1], amount_money:money, converted_money:money, start_date:st_dt, end_date:ed_dt)
          end
        end
      end
    when InitProject::IMPORT_TABLES[3]          # keywordarticles
      headers = CSV.read(csv_file.path, headers: true).headers
      if headers.count != 7
        flash[:error] = "Can't import future projects data. Please check the csv file format"
        redirect_to action: :index and return
      end
      CSV.foreach(csv_file.path, headers:true) do |row|
        columns = row.to_a
        project = Project.where({:site_code=>/^#{columns[0][1]}$/i}).first
        if project.present?
          Keyword.create(project_id:project.id, pri_key:columns[1][1], sec_key:columns[2][1], pri_key_difficult:columns[3][1], sec_key_difficult:columns[4][1], pri_key_monthly:columns[5][1], sec_key_monthly:columns[6][1])
        else
          missed_row << columns[0][1]
        end
      end
    when InitProject::IMPORT_TABLES[4]          # tasks
      headers = CSV.read(csv_file.path, headers: true).headers
      if headers.count != 6
        flash[:error] = "Can't import tasks data. Please check the csv file format"
        redirect_to action: :index and return
      end      
      CSV.foreach(csv_file.path, headers:true) do |row|
        columns = row.to_a
        project = Project.where({:site_code=>/^#{columns[0][1]}$/i}).first
        task = Task.where(project_id:project.id, name:columns[1][1]).first
        st_dt = Date.strptime(columns[3][1],"%d/%m/%Y")
        ed_dt = Date.strptime(columns[4][1],"%d/%m/%Y")
        if project.present?
          if task.present?
            task.update_attributes(duration:columns[2][1], start_date:st_dt, end_date:ed_dt, owners_name:columns[5][1])
          else
            Task.create(project_id:project.id, name:columns[1][1], duration:columns[2][1], start_date:st_dt, end_date:ed_dt, owners_name:columns[5][1])
          end
        else
          missed_row << columns[0][1]
        end
      end
    when InitProject::IMPORT_TABLES[5]          # order&income    
      flash[:error] = "Can't import this data. Please check the csv file format"
      redirect_to action: :index and return
    when InitProject::IMPORT_TABLES[6]          # expenses
      headers = CSV.read(csv_file.path, headers: true).headers
      if headers.count != 6
        flash[:error] = "Can't import expenses data. Please check the csv file format"
        redirect_to action: :index and return
      end
      CSV.foreach(csv_file.path, headers:true) do |row|
        columns = row.to_a
        project = Project.where({:site_code=>/^#{columns[0][1]}$/i}).first
        # expense = Expense.where(project_id:project.id).first
        dt = Date.strptime(columns[3][1],"%d/%m/%Y")
        money = Money.new(columns[4][1].to_f * 100.0, current_user.currency_type)         
        if project.present?
          Expense.create(project_id:project.id, title:columns[1][1], item_type:Expense.item_types.index(columns[2][1]), date:dt, amount_money:money, converted_money:money, payment_method:Expense::payment_methods.index(columns[5][1]))
        else
          missed_row << columns[0][1]
        end
      end
    end
    current_user.init_projects.create(uploaded_date:Time.now, table_name:table, content:"CSV FILE")
    if missed_row.count > 1
      flash[:error] = "upload failed - project site codes don't exist"
    else
      flash[:notice] = "Imported the #{table} csv file."
    end
    redirect_to action: :index
  end

end
