class UserMailer < ActionMailer::Base
  include Devise::Mailers::Helpers  
  default from: "admin@bluadmin.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.expire_email.subject
  #
  def expire_email(user)
    mail(:to => user.email, :subject => "Subscription Cancelled")
  end
  
  def confirmation_email(user)
    mail(:to => user.email, :subject => "Created new account")
  end

  def contact_email(name, email, text)
    @name = name
    @email = email
    @text = text
    mail(:to=> "admin@bluadmin.com", :subject => "Contact")
  end

  # def contact_email(name, email, text)
  #   @name = name
  #   @email = email
  #   @text = text
  #   mail(:to=> email, :subject => "Contact")
  # end
  def adde_new_onwer(owner)
    @owner = owner
    mail(:to=> @owner.email, :subject => "Assigned to task")
  end
  
  def assigned_onwer(owner, task)
    @owner = owner
    @task = task
    mail(:to=> @owner.email, :subject => "Assigned to task")
  end

  def send_alert(user)
    @user = user
    mail(:to => user.email, :subject => "Alert")
  end

  def reset_password(user)
    @user = user
    raw, enc = Devise.token_generator.generate(user.class, :reset_password_token)
    user.reset_password_token   = enc
    user.reset_password_sent_at = Time.now.utc
    user.save(:validate => false)
    @token = raw
    mail(to: @user.email, subject: "Reset Password")
  end


  def send_mail(email, name, subject, body)
    m = Mandrill::API.new "G4_W2P6PhAt6B9zFQ70xRw"
    message = {  
     :subject=> subject,
     :from_name=> 'BluAdmin',
     :text=>body,
     :to=>[  
       {  
         :email=> email,
         :name=> name
       }  
     ],  
     :html=>"<html><h1>Hi #{name}</h1><strong>#{body}</strong></html>",  
     :from_email=>"admin@bluadmin.com"  
    }  
    sending = m.messages.send message  
    puts sending
  end
end
