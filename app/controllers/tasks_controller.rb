class TasksController < ApplicationController
  before_filter :authenticate_user!
  def index
    limit = params[:page_limit].present? ? params[:page_limit].to_i : 10
    @tasks = current_user.tasks.paginate(page: params[:page], :per_page => limit)    
    if params[:fid].present?
      ids = params[:fid].chop!
      afs = AttachFile.in(id:ids.split(","))
      afs.destroy_all
    end
  end

  def new
    @task = Task.new
  end
  def create    
    st_dt = DateTime.strptime(params[:task][:start_date], "%d/%m/%Y")
    ed_dt = st_dt + params[:task][:duration].to_i.days    
    params[:task][:end_date] = ed_dt.strftime("%Y-%m-%d")
    @task = Task.new(params[:task].permit(:project_id, :name, :description, :owner, :duration, :start_date, :end_date))
    @task.attach_file_ids = params[:attach_files].join(",") if params[:attach_files].present?
    project = @task.project
    respond_to do |format|
      if @task.save
        UserMailer.assigned_onwer(@task.owner, @task).deliver
        if params[:task][:p_id].present?
          format.html { redirect_to edit_individual_project_path(project) + "?task_page=1"}
        else
          format.html { redirect_to action: :index }
        end        
        format.json { render json: @task, status: :created, location: @task}
        flash[:success] = 'New task was created successfully'
      else
        format.html { render action: :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end      
    end
  end

  def edit
    @task = Task.find(params[:id])
  end
  def update    
    @task = Task.find(params[:id])
    project = @task.project #Project.find(params[:task][:project_id])
    st_dt = DateTime.strptime(params[:task][:start_date], "%d/%m/%Y")
    ed_dt = st_dt + params[:task][:duration].to_i.days    
    params[:task][:end_date] = ed_dt.strftime("%Y-%m-%d")
    
    respond_to do |format|
      if @task.update_attributes(params[:task].permit(:name, :description, :owner, :duration, :start_date, :end_date))
        @task.update_attribute(:attach_file_ids, params[:attach_files].join(",")) if params[:attach_files].present?
        UserMailer.assigned_onwer(@task.owner, @task).deliver
        if params[:task][:p_id].present?
          format.html { redirect_to edit_individual_project_path(project)+"?task_page=1"}
        else
          format.html { redirect_to action: :index }
        end
        format.json { render json: @task, status: :updated, location: @task}
        flash[:success] = 'Task was updated successfully'
      else
        format.html { redirect_to request.referer }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def show
    @task = Task.find(params[:id])
  end


  def mark_as_complete
    @task = Task.find(params[:id])
    @task.update_attribute(:completed, true)
    redirect_to edit_individual_project_path(@task.project)
  end

  def mark_as_uncomplete
    @task = Task.find(params[:id])
    @task.update_attribute(:completed, false)
    redirect_to edit_individual_project_path(@task.project)
  end

  def add_new_owner
    owner = current_user.owners.build(params[:owner].permit(:name, :email))
    if owner.save
      # UserMailer.adde_new_onwer(owner).deliver
      data = {:success => [owner.id.to_s, owner.name]}
    else
      data = {:failure => owner.errors.messages.to_json}
    end
    render :json => data
  end

  def destroy    
    @task = Task.find(params[:id])
    @task.destroy
    if params[:pid].present?
      redirect_to edit_individual_project_path(params[:pid]) + "?task_page=1" if @task.destroy
    else
      redirect_to action: :index
    end
  end
end
