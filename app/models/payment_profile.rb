class PaymentProfile
  include Mongoid::Document
  include Mongoid::Timestamps

  field :refund_id,         type: String
  field :price_subtotal,    type: String
  field :price_tax,         type: String
  field :price_refund,      type: String
  field :price_total,       type: String
    
  field :first_name,        type: String
  field :last_name,         type: String
  field :street_address,    type: String
  field :city,              type: String
  field :region,            type: String 
  field :post_code,         type: String
  field :country,           type: String

  field :exp_month,         type: Integer
  field :exp_year,          type: Integer
  
  field :ccard_last4,       type: String
  field :status,            type: String  

  belongs_to :user
end
