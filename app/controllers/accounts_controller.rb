class AccountsController < ApplicationController
  before_filter :authenticate_user!
  def new
    @account = Account.new
  end

  def create
    @project = Project.find(params[:account][:project_id])
    @account = Account.new(params[:account].permit(:project_id, :type, :user_name, :password, :url))
    respond_to do |format|
      if @account.save
        format.html { redirect_to edit_individual_project_path(@project) + "?account_page=1"}
        format.json { render json: @account, status: :created, location: @account}
        flash[:notice] = 'Account was created successfully'
      else
        format.html { render action: :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end      
    end

  end

  def edit
    @account = Account.find(params[:id])
  end

  def update
    @account = Account.find(params[:id])
    @project = Project.find(params[:account][:project_id])
    respond_to do |format|
      if @account.update_attributes(params[:account].permit(:project_id, :type, :user_name, :password, :url))
        format.html { redirect_to edit_individual_project_path(@project)}
        format.json { render json: @account, status: :updated, location: @account}
        flash[:notice] = 'Account was updated successfully'
      else
        format.html { redirect_to request.referer }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @account = Account.find(params[:id])    
    redirect_to edit_individual_project_path(params[:pid]) if @account.destroy
  end
end
