class Option
  include Mongoid::Document
  include Mongoid::Timestamps

  field :option_name,           type: String
  field :option_value,          type: String

  belongs_to :user

  validates_uniqueness_of :option_name, scope: :user_id

  def self.find_by_option_name user, option_name
    user.options.where(option_name:option_name).first
  end

  def self.create_by_option_name option_name
    option = Option.where(option_name:option_name).first
    option.present? ? option : Option.new(option_name:option_name)
  end

  def self.option_value user, option_name
    option = user.options.where(option_name:option_name).first
    if option.present?
      if option.option_value == "true"
        true
      elsif option.option_value == "false"
        false
      else
        option.option_value
      end        
    else
      false
    end
  end
end
