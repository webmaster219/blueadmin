class ExpensesController < ApplicationController
  before_filter :authenticate_user! 
  require 'money/bank/google_currency'
  def index
    limit = params[:page_limit].present? ? params[:page_limit].to_i : 10
    @expenses = current_user.expenses.order_by("created_at DESC").paginate(page: params[:page], :per_page => limit)
    
    expense_values = []
    # project_ids = current_user.projects.map(&:id)
    [*1..12].each do |m|
      expense = current_user.expenses.amount_of_month Time.now.year, m
      expense_values[m-1] = expense
    end
    
    @chart1 = LazyHighCharts::HighChart.new('column') do |f|
      f.title(:text => "Expenses")
      f.xAxis(:categories => Date::MONTHNAMES.reject{|m| m.nil?}.map{|m| m[0..2]})
      f.series(:name => "Expense", :yAxis => 0, :data => expense_values)

      # f.yAxis [
      #   {:title => {:text => "Expense", :margin => 70} },
      # ]

      f.legend(:align => 'right', :verticalAlign => 'top', :y => -10, :x => 10, :layout => 'vertical', width: 0,)
      f.chart({type: 'column', marginRight: 15, marginLeft: 45,marginBottom: 30, :defaultSeriesType=>"column" })
    end

    income_values = []
    [*1..12].each do |m|        
      income = current_user.monthly_incomes.where(month:m).map(&:incomes).sum
      income_values[m-1] = income      
    end
    @chart2 = LazyHighCharts::HighChart.new('basic_line') do |f|
      f.title(:text => "Profit VS Expenses")
      f.xAxis(:categories =>  Date::MONTHNAMES.reject{|m| m.nil?}.map{|m| m[0..2]})
      f.series(:name => "Expense", :yAxis => 0, :data => expense_values)
      f.series(:name => "Income", :yAxis => 0, :data => income_values)

      # f.yAxis [
      #   {:title => {:text => "Expense", :margin => 70} },
      #   {:title => {:text => "Income"}, :opposite => true},
      # ]

      f.legend(:align => 'right', :verticalAlign => 'top', :y => -10, :x => 10, :layout => 'vertical', width: 0,)
      f.chart({type: 'line', marginRight: 15, marginLeft: 45,marginBottom: 30, :defaultSeriesType=>"column" })
    end
    if params[:fid].present?
      ids = params[:fid].chop!
      afs = AttachFile.in(id:ids.split(","))
      afs.destroy_all
    end
  end

  def new
    @expense = Expense.new
  end

  def create
    Money.default_bank = Money::Bank::GoogleCurrency.new
    date = DateTime.strptime(params[:expense][:date], "%d/%m/%Y")
    params[:expense][:date]=date.strftime("%Y-%m-%d 05:50:50")
    params[:expense][:recurring] = params[:expense][:recurring] == "1"
    @expense = Expense.new(params[:expense].permit(:project_id, :item_type, :date, :payment_method, :budget_pot, :title, :description, :recurring))
    @expense.amount_money     = Money.new(params[:expense][:amount_money].to_f * 100.0, params[:expense][:currency_type])
    @expense.converted_money  = @expense.amount_money.exchange_to(current_user.currency_type.to_sym)
    @expense.attach_file_ids  = params[:attach_files].join(",") if params[:attach_files].present?
    @expense.original = true
    respond_to do |format|
      if @expense.save
        format.html { redirect_to action: :index}        
        format.json { render json: @expense, status: :created, location: @expense}
        flash[:notice] = 'Expense was created successfully'
      else
        format.html { render action: :new }
        format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @expense = Expense.find(params[:id])
  end

  def update
    params[:expense][:recurring] = params[:expense][:recurring] == "1"
    Money.default_bank = Money::Bank::GoogleCurrency.new
    date = DateTime.strptime(params[:expense][:date], "%d/%m/%Y")
    params[:expense][:date]=date.strftime("%Y-%m-%d 05:50:50")
    @expense = Expense.find(params[:id])
    @expense.assign_attributes(params[:expense].permit(:project_id, :item_type, :date, :payment_method, :budget_pot, :title, :description, :recurring))
    @expense.amount_money     = Money.new(params[:expense][:amount_money].to_f * 100.0, params[:expense][:currency_type])
    @expense.converted_money  = @expense.amount_money.exchange_to(current_user.currency_type.to_sym)
    @expense.attach_file_ids  = params[:attach_files].join(",") if params[:attach_files].present?
    respond_to do |format|
      if @expense.save
        format.html { redirect_to action: :index}        
        format.json { render json: @expense, status: :created, location: @expense}
        flash[:notice] = 'Expense was created successfully'
      else
        format.html { render action: :edit }
        format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @expense = Expense.find(params[:id])
  end

  def destroy
    @expense = Expense.find(params[:id])
    if @expense.destroy
      redirect_to action: :index
      flash[:notice] = 'Expense was deleted'
    end
  end
end
