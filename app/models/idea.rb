class Idea
  include Mongoid::Document
  include Mongoid::Timestamps

  field :number,                          type: Integer
  field :primary_keyword,                 type: String
  field :second_keyword,                  type: String
  field :keyword_difficulty,              type: String
  field :monthly_search,                  type: String

  has_one :article
  belongs_to :project
end
