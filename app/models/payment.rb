class Payment
  include Mongoid::Document
  include Mongoid::Timestamps

  CURRENCY_TYPES= %w[GBP USD AUD EUR]

  belongs_to :user
end
