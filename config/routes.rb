Bluadmin::Application.routes.draw do  
  captcha_route
  mount StripeEvent::Engine => '/stripe'
  
  namespace :admin do
    resources :users, only: [:index]
    get "send_to_reset_password_email/:id" => "users#send_to_reset_password_email", as: 'send_to_reset_password_email'
    get "setting"             => "users#setting"
    post "new_item"           => "users#new_item"
    match "update_option"     => 'users#update_setting_option',        via: [:post, :patch]
    delete "delete_items/:id"     => "users#delete_items", as: "delete_items"
    
  end
  
  get 'home/index'
  get 'home/about'
  match 'home/contact'             => 'home#contact',        via: [:get, :post]
  get 'home/features'
  get 'home/pricing'

  resources :expenses
  resources :payment_profiles
  resources :projects  
  get 'projects/make_live/:id'                  => 'projects#make_live',   as: 'make_live'

  resources :individual_projects
  get "get_year_data"                           => 'individual_projects#get_year_data'
  get "get_year_bulk_data"                      => 'individual_projects#get_year_bulk_data'

  resources :accounts
  resources :articles
  resources :keywords 
  resources :tasks

  resources :attach_files, only:[:create, :destroy]

  post 'tasks/mark_as_complete/:id'             => 'tasks#mark_as_complete', as: 'mark_as_complete'
  post 'tasks/mark_as_uncomplete/:id'           => 'tasks#mark_as_uncomplete', as: 'mark_as_uncomplete'
  post 'add_new_owner'                          => 'tasks#add_new_owner', as: 'add_new_owner'

  devise_for :users, :controllers => { registrations: "users/registrations", sessions: "users/sessions" }

  get 'users/index'
  get 'users/setting'

  post 'users/add_card'
  delete 'users/destroy'          => 'users#destroy'
  match 'user/update'             => 'users#update',        via: [:post, :patch]
  
  get 'user/pricing/:id'          => 'users#pricing',       as: 'user_pricing'
  post 'user/purchase'            => 'users#purchase'
  get 'user/thanks'               => 'users#thanks'
  get 'user/money_converting'     => 'users#money_converting'

  post '/tinymce_assets' => 'projects#upload_image'
  
  post '/home/set_option'         => 'home#set_option'

  resources :init_projects
  post '/projects/import_csv'     => 'init_projects#import_csv'
  
  root to: 'home#index'
end
