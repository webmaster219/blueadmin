// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

jQuery(function($) {
  $('#checkout_form').submit(function(event) {
    var $form = $(this);
    Stripe.setPublishableKey('pk_test_1dvsMd85Bum8CQHmzzKkHKql');
    // Disable the submit button to prevent repeated clicks
    $form.find('button').prop('disabled', true);

    Stripe.card.createToken($form, stripeResponseHandler);

    // Prevent the form from submitting with the default action
    return false;
  });
});

var stripeResponseHandler = function(status, response) {
  var $form = $('#checkout_form');

  if (response.error) {
    // Show the errors on the form
    $form.find('.payment-errors').text(response.error.message).css("padding","5px 10px");
    $form.find('button').prop('disabled', false);
  } else {
    // token contains id, last4, and card type
    var token = response.id;
    // Insert the token into the form so it gets submitted to the server
    $form.append($('<input type="hidden" name="stripeToken" />').val(token));
    // and submit
    $form.get(0).submit();
  }
};