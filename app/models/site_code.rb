class SiteCode
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,        type: String

  has_many :users
  has_many :tasks

  def self.find_or_create_by_name(name)
    st = SiteCode.where(name:name).first
    SiteCode.create(name:name) if st.nil?
  end
end
