
Stripe.api_key = ENV['STRIPE_API_TEST_KEY']
STRIPE_PUBLIC_KEY = ENV['STRIPE_PUBLIC_TEST_KEY']

StripeEvent.setup do
  subscribe 'customer.subscription.deleted' do |event|
    user = User.find_by_customer_id(event.data.object.customer)
    user.expire
  end
end
