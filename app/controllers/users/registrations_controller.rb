class Users::RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    token = params[:stripeToken]    
    @user = User.new(params[:user].permit(:first_name, :last_name, :email, :password, :password_confirmation))
    
    if params[:member].present? and User::MEMBER_TYPES.include?(params[:member])
      @user.member_type = User::MEMBER_TYPES.index(params[:member])
    else
      @user.member_type = 0
    end

    if token.nil?
      respond_to do |format|
        format.html { render action: :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
        flash[:error] = "Oh snap! Please check your card number"        
      end
    else
      begin
        coupon = Stripe::Coupon.create(
          :percent_off => 100,
          :duration => 'repeating',
          :duration_in_months => 1,
          :id => @user.id
        )
        
        if params[:member] == User::MEMBER_TYPES[0]
          customer = Stripe::Customer.create(
            :card => token,
            :plan => User::MEMBER_TYPES[@user.member_type],
            :email => @user.email,
            :coupon => coupon,
            :description=> @user.name
          )
        else
          customer = Stripe::Customer.create(
            :card => token,
            :plan => User::MEMBER_TYPES[@user.member_type],
            :email => @user.email,
            :description=> @user.name
          )
        end      

        @user.customer_id = customer.id
        @user.status = 'live'
        if @user.save
          p_profile = @user.build_payment_profile(exp_month: params[:exp_month], exp_year: params[:exp_year], ccard_last4: params[:cvc_number])
          p_profile.save
          @user.owners.create(name:@user.name, email:@user.email)
          sign_in(:user, @user)
          super
          redirect_to user_thanks_path and return
        else
          customer.delete
          respond_to do |format|
            format.html { render action: :new }
            format.json { render json: @user.errors, status: :unprocessable_entity }
          end
        end
      rescue Stripe::CardError => e
        respond_to do |format|
          format.html { render action: :new }
          format.json { render json: @user.errors, status: :unprocessable_entity }
          flash[:error] = "Oh snap! Your payment failed please try again or contact your bank"
        end
      end
    end
  end

  def update
    super
  end

  protected

  def after_sign_up_path_for(resource)
    new_user_session_path
  end

  def after_inactive_sign_up_path_for(resource)
    new_user_session_path
  end
end