class AttachFilesController < ApplicationController
  before_filter :authenticate_user!
  def create
    if current_user.uploaded_file_size > current_user.file_limit      
      render :json => { :errors => "Please change your membership, your limit is #{current_user.file_limit}Mb" }, status: 422  
    else      
      @attach = AttachFile.new(file:params[:file])
      @attach.user_id = current_user.id
      respond_to do |format|
        begin
          if @attach.save
            format.html {
              render :json => @attach.to_jq_upload,:content_type => 'text/html',:layout => false
            }
            format.json { render json: {files: @attach.to_jq_upload}, status: :created, location: @attach }
          else
            format.html { render action: "new" }
            format.json { render json: @attach.errors, status: :unprocessable_entity }
          end
        rescue Exception => exc        
          render :json => { :errors => "Please contact to admin" }, status: 422  
        end
      end
    end  
  end

  
  def destroy
    @attach = AttachFile.find(params[:id])
    @attach.destroy
    render :json=>"success"
  end
end
