class KeywordsController < ApplicationController
  before_filter :authenticate_user!
  def new
    if params[:pid].present?
      project = Project.find(params[:pid])
      @keyword = project.keywords.build
    else
      @keyword = Keyword.new
    end
    
  end
  def create
    #project = Project.find(params[:keyword][:project_id])
    @keyword = Keyword.new(params[:keyword].permit(:project_id, :pri_key, :sec_key, :pri_key_difficult, :sec_key_difficult, :pri_key_monthly, :sec_key_monthly))
    
    respond_to do |format|
      if @keyword.save
        if params[:keyword][:p_id].present?
          format.html { redirect_to edit_individual_project_path(params[:keyword][:p_id])+"?article_page=1"}
        else
          format.html { redirect_to articles_path}
        end
        
        format.json { render json: @keyword, status: :created, location: @keyword}
        flash[:notice] = 'New keyword was created successfully'
      else
        format.html { render action: :new }
        format.json { render json: @keyword.errors, status: :unprocessable_entity }
      end      
    end
  end

  def edit
    @keyword = Keyword.find(params[:id])
  end

  def update    
    @keyword = Keyword.find(params[:id])
    respond_to do |format|
      if @keyword.update_attributes(params[:keyword].permit(:project_id, :pri_key, :sec_key, :pri_key_difficult, :sec_key_difficult, :pri_key_monthly, :sec_key_monthly))
        if params[:keyword][:p_id].present?
          format.html { redirect_to edit_individual_project_path(@keyword.project)+"?article_page=1"}
        else
          format.html { redirect_to articles_path}
        end
        format.json { render json: @keyword, status: :updated, location: @keyword}
        flash[:notice] = 'keyword was updated successfully'
      else
        format.html { redirect_to request.referer }
        format.json { render json: @keyword.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @keyword = Keyword.find(params[:id])
    @keyword.destroy
    redirect_to edit_individual_project_path(params[:pid]) if @keyword.destroy
  end
end
