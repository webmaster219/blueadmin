class HomeController < ApplicationController
  before_filter :authenticate_user!, only: [:set_option]
  def index    
    if current_user.present?
      @projects = current_user.live_projects.live_projects.paginate(page: params[:page], :per_page => 10)
      step = 0
      profits = []
      if Option.option_value(current_user, :all_site_profit)
        @chart1 = LazyHighCharts::HighChart.new('basic_line') do |f|
          f.title(:text => "All Projects")
          f.xAxis(:categories => Date::MONTHNAMES.reject{|m| m.nil?}.map{|m| m[0..2]})
          if current_user.live_projects.count > 0
            current_user.live_projects.each do |project|
              if project.have_figure?
                values = []
                [*1..12].each do |m|
                  values[m-1] = project.profit_loss_by_month(Time.now.year, m)
                end
                profits << values
                f.series(:name => project.site_code, :yAxis => 0, :data => profits[step])
                step += 1
              end
              # break if step == 12
            end
          else
            f.series(:name => ['none'], :yAxis => 0, :data => [0])
          end
          f.legend(:align => 'right', :verticalAlign => 'top', :y => 40, :x => 10, :layout => 'vertical', width: 0,)
          f.chart({type: 'line', marginRight: 15, marginLeft: 50,marginBottom: 30, marginTop: 60, :defaultSeriesType=>"column" })
        end
      end

      income_values = []
      target_values = []
      if Option.option_value(current_user, :top_earning)
        [*1..12].each do |m|
          income = current_user.monthly_incomes.incomes_of_month(Time.now.year, m)
          target = current_user.live_projects.target_of_month(Time.now.year, m)
          income_values[m-1] = income
          target_values[m-1] = target
        end
        @chart2 = LazyHighCharts::HighChart.new('basic_line') do |f|
          f.title(:text => "Top Earning")
          f.xAxis(:categories =>  Date::MONTHNAMES.reject{|m| m.nil?}.map{|m| m[0..2]})
          f.series(:name => "Income", :yAxis => 0, :data => income_values)
          f.series(:name => "Target", :yAxis => 0, :data => target_values)
          f.legend(:align => 'right', :verticalAlign => 'top', :y => 0, :x => 10, :layout => 'vertical', width: 0,)
          f.chart({type: 'line', marginRight: 15, marginLeft: 50,marginBottom: 30, marginTop: 60, :defaultSeriesType=>"column" })
        end
      end

      income_values = []
      expense_values = []
      if Option.option_value(current_user, :all_site_expenses)
        [*1..12].each do |m|
          income = current_user.monthly_incomes.incomes_of_month(Time.now.year,m)
          expense = current_user.expenses.amount_of_month(Time.now.year, m)
          income_values[m-1] = income
          expense_values[m-1] = expense
        end
        @chart3 = LazyHighCharts::HighChart.new('basic_line') do |f|
          f.title(:text => "Expenses V Earnings")
          f.xAxis(:categories =>  Date::MONTHNAMES.reject{|m| m.nil?}.map{|m| m[0..2]})
          f.series(:name => "Expenses", :yAxis => 0, :data => expense_values)
          f.series(:name => "Income", :yAxis => 0, :data => income_values)

          f.legend(:align => 'right', :verticalAlign => 'top', :y => 0, :x => 10, :layout => 'vertical', width: 0,)
          f.chart({type: 'line', marginRight: 15, marginLeft: 50,marginBottom: 30, marginTop: 60, :defaultSeriesType=>"column" })
        end
      end

      if Option.option_value(current_user, :monthly_income_monthly_target)
        m_incomes = current_user.monthly_incomes.incomes_of_month(Time.now.year, Time.now.month)
        m_targets = current_user.live_projects.target_of_month(Time.now.year, Time.now.month)
        @chart4 = LazyHighCharts::HighChart.new('column') do |f|
          f.title(:text => "MonthlyIncome VS MonthlyTarge")
          f.xAxis(:categories => ['Monthly Income', 'Monthly Target'])
          f.series(:name => "Value", :yAxis => 0, :data => [m_incomes, m_targets])

          f.legend(:align => 'right', :verticalAlign => 'top', :y => -10, :x => 10, :layout => 'vertical', width: 0,)
          f.chart({type: 'column', marginRight: 15, marginLeft: 50,marginBottom: 30, :defaultSeriesType=>"column" })
        end
      end

      if Option.option_value(current_user, :last_year_current_year)
        lst_profit = current_user.live_projects.profit_loss_by_year(Time.now.year.to_i - 1)
        cur_profit = current_user.live_projects.profit_loss_by_year(Time.now.year.to_i)
        @chart5 = LazyHighCharts::HighChart.new('column') do |f|
          f.title(:text => "#{Time.now.year.to_i - 1} Profit/Loss VS #{Time.now.year} Profit/Loss")
          f.xAxis(:categories => ["#{Time.now.year.to_i - 1 }", "#{Time.now.year}"])
          f.series(:name => "Profit/Loss", :yAxis => 0, :data => [lst_profit, cur_profit] )

          f.legend(:align => 'right', :verticalAlign => 'top', :y => -10, :x => 10, :layout => 'vertical', width: 0,)
          f.chart({type: 'column', marginRight: 15, marginLeft: 50,marginBottom: 30, :defaultSeriesType=>"column" })
        end
      end

      if Option.option_value(current_user, :top_expenses)
        
        items = current_user.expenses.all.map(&:item_type).uniq
        values = []
        item_value = items.count == 0 ? [0] : []
        item_label = items.count == 0 ? ['none'] : []
        
        items.each do |item|
          exp = current_user.expenses.where(item_type:item)
          if exp.count > 0
            values << [exp.map{|ex| ex.amount}.sum, Expense.item_types[item.to_i]]
          end
        end
        
        if values.count > 0
          values = values.sort{|x, y| y <=> x}
          values[0..4].each do |value|
            item_value << value[0]
            item_label << value[1]
          end
        end

        lst_profit = current_user.live_projects.profit_loss_by_year(Time.now.year.to_i - 1)
        cur_profit = current_user.live_projects.profit_loss_by_year(Time.now.year.to_i)
        @chart6 = LazyHighCharts::HighChart.new('column') do |f|
          f.title(:text => "Top Expenses")
          f.xAxis(:categories => item_label)
          f.series(:name => "Expense", :yAxis => 0, :data => item_value)

          f.legend(:align => 'right', :verticalAlign => 'top', :y => -10, :x => 10, :layout => 'vertical', width: 0,)
          f.chart({type: 'column', marginRight: 15, marginLeft: 45,marginBottom: 30, :defaultSeriesType=>"column" })
        end
      end
    end
  end

  def about
  end

  def contact
    if request.post?
      if valid_captcha?(params[:captcha])
        UserMailer.contact_email(params[:name], params[:email], params[:text]).deliver
        flash[:notice] = "Sent email successfully"
        redirect_to home_contact_path
      else
        flash[:error] = "Please check capcha again"
        redirect_to home_contact_path
      end
    end
  end

  def pricing
  end

  def features
  end

  def set_option
    option_list = %w[all_site_profit top_earning all_site_expenses monthly_income_monthly_target last_year_current_year top_expenses]
    if params[:rest_to_default] == "1"      
      option_list.each do |key, value|
        opt = current_user.options.where(option_name:key).first
        opt.update_attribute(:option_value, false) if opt.present?
      end
      option_list[0..2].each do |key, value|
        opt = current_user.options.where(option_name:key).first
        opt.update_attribute(:option_value, true) if opt.present?
      end
    else
      params[:h_option].each do |key, value|
        option = current_user.options.where(option_name:key).first
        if option.present?
          option.update_attribute(:option_value, value)
        else
          current_user.options.create(option_name:key,option_value:value)
        end
      end
    end    
    redirect_to root_path
  end
end
