# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'Create new admin account'
user = User.create(first_name:'Admin', last_name:'Tester', email:'admin@email.com', password:'admin321', password_confirmation:'admin321')

SiteCode.find_or_create_by_name('SCR')
SiteCode.find_or_create_by_name('SMR')
SiteCode.find_or_create_by_name('KGR')
SiteCode.find_or_create_by_name('BBY')
SiteCode.find_or_create_by_name('BMR')

puts 'Option create'

option = Option.create_by_option_name(:expense_items)
option.update_attribute(:option_value, "Product,Affliate Payments,Advertising,Aweber,Bank/Finance,Charges,Broadband,Bookeeping,Business Entertainment,Computer Hardware,Copywriting,Content,Domains,Exhibitions/Conferences,Hosting,Graphics,Insurance,Legal,Licenising,Motor Expenses,Mechant Fees,Office Rental,Office Equipment,Staff Training,Plugins,SaaS,Software,SocialAdr,Staffing,Subscriptions,Recurring Payments,Sundries,Training,Themes,Telephone,Travel,Wage,Pay,SEM,SEO,CPM,CPE,CPC,CPA,CPV,CPL,PPC,VA,GA,Website")

option = Option.create_by_option_name(:expense_payments)
option.update_attribute(:option_value, "Cheque,Personal Paypal Acc,Business Paypal Acc,Amazon Pay,Google Checkout,2Checkout,Business Credit Card,Personal Debit,Card,Personal Credit,Card,Skrill,Other")

option = Option.create_by_option_name(:project_types)
option.update_attribute(:option_value, "Website,Joint Venture,Mobile App,E-Commerce Site,PBN,Offline Business,Email Marketing,Kindle Books,Forum,Duel,Product Reviews,Solo Ads,PPC,SaaS,Podcast,Pluigins,Themes,Domains,Links,Copywriting")

option = Option.create_by_option_name(:moneitions)
option.update_attribute(:option_value,"Amazon Affliate,Adsense Affliate,Affiliate Marketing,Content Marketing,Email Marketing,E-Commerce,Lead Gen,Mobile Marketing,Offline Marketing,Social Media,Marketing,Video,Marketing,PPC,SEM,SEO,CPM,CPE,CPC,CPA,CPV,CPL,PayPerInstall,ClickBank")

puts 'Option create end'