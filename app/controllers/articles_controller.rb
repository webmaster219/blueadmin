class ArticlesController < ApplicationController
  before_filter :authenticate_user!
  def index
    limit = params[:page_limit].present? ? params[:page_limit].to_i : 10
    @articles = current_user.articles.paginate(page: params[:page], :per_page => limit)
    @keywords = current_user.keywords.paginate(page: params[:page], :per_page => limit)
  end
  
  def new
    @article = Article.new    
  end
  
  def create
    project = Project.find(params[:article][:project_id])
    @article = project.articles.build(params[:article].permit(:name, :pri_key, :sec_key, :lsi))
    
    respond_to do |format|
      if @article.save
        format.html { redirect_to edit_individual_project_path(project)+"?article_page=1"}
        format.json { render json: @article, status: :created, location: @article}
        flash[:notice] = 'New Article was created successfully'
      else
        format.html { render action: :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end      
    end
  end

  def edit
    @article = Article.find(params[:id])
  end
  def update    
    @article = Article.find(params[:id])
    project = @article.project #Project.find(params[:article][:project_id])
    respond_to do |format|
      if @article.update_attributes(params[:article].permit(:name, :pri_key, :sec_key, :lsi))
        format.html { redirect_to edit_individual_project_path(project)+"?article_page=1"}
        format.json { render json: @article, status: :updated, location: @article}
        flash[:notice] = 'Article was updated successfully'
      else
        format.html { redirect_to request.referer }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    redirect_to edit_individual_project_path(params[:pid]) if @article.destroy
  end
end
