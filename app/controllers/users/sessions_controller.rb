class Users::SessionsController < Devise::SessionsController
  def create
    user = User.where(email:params[:user][:email]).first
    if user.present? and !user.is_admin? and user.payment_profile.nil?
      flash[:notice] = "Please select package"
      redirect_to user_pricing_path(user.id) and return
    elsif user.present? and user.status == 'expired'
      flash[:notice] = "Your account was expired, please contact to admin "
      redirect_to  home_contact_path  and return
    else
      super
    end
  end
    
end