require 'carrierwave/mongoid'

CarrierWave.configure do |config|
  # if Rails.env.production?
    config.fog_credentials = {
      :provider                 => 'AWS',
      :aws_access_key_id        => ENV['S3_API_KEY_ID'],
      :aws_secret_access_key    => ENV['S3_API_SECRET_KEY'],
      :region                   => ENV['S3_API_REGION'],
      :host                     => 's3.amazonaws.com',
      :endpoint                 => 'https://s3.amazonaws.com' # optional, defaults to nil
    }
    config.fog_directory        = ENV['S3_API_BUCKET']
    config.fog_public           = false                                   # optional, defaults to true
    config.fog_authenticated_url_expiration = 24*60*60
    config.fog_attributes       = {'Cache-Control'=>'max-age=315576000'}  # optional, defaults to {}

  # else    
  #   config.storage              = :file
  #   config.root                 = Rails.root.join('tmp')
  #   config.cache_dir            = "uploads"
  # end

end