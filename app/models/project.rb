class Project
  include Mongoid::Document
  include Mongoid::Timestamps
  
  # mount_uploader :attach, ProjectAttachUploader

  COMMON_YEAR_DAYS_IN_MONTH = [nil, 31, 28, 31, 30, 31, 30, 31, 31,30,31,30,31]
  PROJECT_TYPES = %w[Website Joint\ Venture Mobile\ App E-Commerce\ Site PBN Offline\ Business Email\ Marketing Kindle\ Books Forum Duel Product\ Reviews Solo\ Ads PPC SaaS Podcast Pluigins Themes Domains Links Copywriting]
  MONEISTATIONS = %w[Amazon\ Affliate Adsense\ Affliate Affiliate\ Marketing Content\ Marketing Email\ Marketing E-Commerce Lead\ Gen Mobile\ Marketing Offline\ Marketing Social\ Media Marketing Video Marketing
# PPC SEM SEO CPM CPE CPC CPA CPV CPL PayPerInstall ClickBank]
  
  field :name,                type: String
  field :site_code,           type: String
  field :domain,              type: String
  
  field :start_date,          type: Date, default:Time.now-3.days
  field :end_date,            type: Date, default:Time.now+3.days

  field :type,                type: String
  field :monetisation,        type: String

  field :description,         type: String
  field :attach,              type: String
  
  # field :budget,              type: Integer, default: 0
  
  field :amount_money,        type: Money
  field :converted_money,     type: Money
  
  field :live_date,           type: Date
  field :active,              type: Boolean,  default: false
  field :monthly_target,      type: Float,    default: 0
  #field :article,             type: Integer
  
  field :attach_file_ids,     type: String

  belongs_to :user
  
  has_many :time_spents,        dependent: :destroy
  has_many :monthly_orders,     dependent: :destroy
  has_many :monthly_incomes,    dependent: :destroy

  has_many :accounts,           dependent: :destroy
  has_many :articles,           dependent: :destroy
  has_many :keywords
  has_many :tasks,              dependent: :destroy
  
  has_many :expenses,           dependent: :destroy
  # has_many :attach_files,       :as => :attachable,       dependent: :destroy
  

  validates_presence_of :name, :type, :monetisation, :amount_money, :site_code
  validates_uniqueness_of :site_code, scope: :user_id
  validate :end_date_validate

  scope :future_projects, ->{where(active:false)}
  scope :live_projects, ->{where(active:true)}
  
  def available_to_live
    if end_date < DateTime.now.to_date
      false
    else
      true
    end
  end
  def attach_files
    if attach_file_ids.present?
      ids = attach_file_ids.split(",")
      AttachFile.in(id:ids)
    else
      []
    end
  end
  
  def have_figure?
    return profit_loss == 0 ? false : true
  end

  def budget
    converted_money.present? ? converted_money.fractional/100.0 : amount_money.fractional/100.0
  end
  def currency_symbol
    money = Money.new(1,user.currency_type)
    converted_money.present? ? converted_money.currency.symbol : money.currency.symbol
  end

  def monetisation_name
    #MONEISTATIONS[monetisation]
    monetisation
  end

  def type_name
    #Project::PROJECT_TYPES[type] if type.present?
    type
  end  

  def profit_loss_by_month year, month
    incomes = monthly_incomes.incomes_of_month(year, month)
    expense = expenses.amount_of_month(year, month)
    incomes - expense
  end

  def profit_loss
    incomes = all_income
    expense = expenses.map(&:amount).sum
    (incomes - expense).round(2)
  end

  def monthly_income
  end

  def yearly_income(year)
    monthly_incomes.where(year:year).map(&:incomes).sum
  end

  def all_income
    monthly_incomes.map(&:incomes).sum
  end

  def total_orders
    monthly_orders.map(&:orders).reject{|o| o == nil}.sum
  end

  def self.target_of_month(year, month)
    targets = where("return this.start_date.getFullYear() == #{year.to_i} && this.start_date.getMonth() == #{month.to_i-1}")
    if targets.count > 0
      tt = targets.map{|tt| tt.monthly_target}
      tt.sum      
    else
      0
    end
  end

  def has_orders?
    if monetisation == "Amazon Affliate" or monetisation == "E-Commerce" or monetisation == "Lead Gen"
      true
    else
      false
    end
  end

  def self.budget_of_year year
    budget = []
    st_dt = Date.new(year, 1,1)
    ed_dt = Date.new(year, 12, 31)
    projects = where(:start_date=>{:$lte=>ed_dt, :$gt => st_dt})
    
    projects.each do |prj|
      budget << prj.budget
    end    
    # projects = where("return this.start_date.getFullYear() == #{year.to_i}")
    # projects.map(&:amount).sum
    (budget.sum).round(2)
  end

  def self.projects_of_month month    
    year = Time.now.year
    st_dt = Date.new(year, month,1)
    ed_dt = Date.new(year, Project::COMMON_YEAR_DAYS_IN_MONTH[month], 30)
    where(:start_date=>{:$lte=>ed_dt, :$gt => st_dt})
  end

  def self.projects_of_year year
    st_dt = Date.new(year, 1,1)
    ed_dt = Date.new(year, 12, 31)
    where(:start_date=>{:$lte=>ed_dt, :$gt => st_dt})
  end

  def self.profit_loss_by_year year
    profit = []
    projects = self.projects_of_year year
    projects.each do |prj|
      profit << prj.profit_loss
    end
    (profit.sum).round(2)
  end

  def budget_of_expenses
    expenses.map(&:amount).sum
  end

  def self.project_types
    option = Option.create_by_option_name(:project_types)
    if option.option_value.present?
      option.option_value.split(",")
    else
      []
    end
  end

  def self.moneitions
    option = Option.create_by_option_name(:moneitions)
    if option.option_value.present?
      option.option_value.split(",")
    else
      []
    end
  end

  private  
  def end_date_validate
    if start_date.present? and end_date.present?
      if start_date > end_date
        errors.add(:end_date, "wrong, end_date should not be large than start_date ")
      end
    end
  end
end
