class MonthlyOrder
  include Mongoid::Document
  include Mongoid::Timestamps

  # mount_uploader :attach, TaskAttachUploader
  field :year,                  type: Integer  
  field :month,                 type: Integer  
  field :orders,                type: Integer
    
  belongs_to :project
  validates_presence_of :project_id, :year, :month, :orders
end
