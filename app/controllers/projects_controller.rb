class ProjectsController < ApplicationController
  before_filter :authenticate_user!
  require 'money/bank/google_currency'
  def index
    limit = params[:page_limit].present? ? params[:page_limit].to_i : 10
    @projects = current_user.future_projects.all.paginate(page: params[:page], :per_page => limit)
    if params[:fid].present?
      ids = params[:fid].chop!
      afs = AttachFile.in(id:ids.split(","))
      afs.destroy_all
    end
  end

  def new
    @project = Project.new
  end

  def create
    Money.default_bank = Money::Bank::GoogleCurrency.new
    st_dt = Date.strptime(params[:project][:start_date],"%d/%m/%Y")
    ed_dt = Date.strptime(params[:project][:end_date],"%d/%m/%Y")
    params[:project][:start_date] = st_dt.strftime("%Y-%m-%d")
    params[:project][:end_date] = ed_dt.strftime("%Y-%m-%d")
    @project = current_user.projects.build(params[:project].permit(:site_code, :name, :domain, :start_date, :end_date, :type, :monetisation, :budget, :description, :monthly_target))    
    @project.attach_file_ids = params[:attach_files].join(",") if params[:attach_files].present?
    @project.amount_money     = Money.new(params[:project][:amount_money].to_f * 100.0, params[:project][:currency_type])
    @project.converted_money  = @project.amount_money.exchange_to(current_user.currency_type.to_sym)
    @project.site_code = @project.site_code.upcase
    respond_to do |format|
      if @project.save
        format.html { redirect_to action: :index}        
        format.json { render json: @project, status: :created, location: @project}
        flash[:notice] = 'Project was created successfully'
      else
        format.html { render action: :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end
  def edit
    @project = Project.find(params[:id])
  end

  def update
    Money.default_bank = Money::Bank::GoogleCurrency.new
    @project = Project.find(params[:id])
    st_dt = Date.strptime(params[:project][:start_date],"%d/%m/%Y")
    ed_dt = Date.strptime(params[:project][:end_date],"%d/%m/%Y")
    params[:project][:start_date] = st_dt.strftime("%Y-%m-%d")
    params[:project][:end_date] = ed_dt.strftime("%Y-%m-%d")
    
    respond_to do |format|
      @project.assign_attributes(params[:project].permit(:site_code, :name, :domain, :start_date, :end_date, :type , :monetisation, :monthly_target, :description))
      @project.amount_money     = Money.new(params[:project][:amount_money].to_f * 100.0, params[:project][:currency_type])
      @project.converted_money  = @project.amount_money.exchange_to(current_user.currency_type.to_sym)
      @project.attach_file_ids = params[:attach_files].join(",") if params[:attach_files].present?
      if @project.save
        if @project.active
          format.html { redirect_to individual_projects_path}
        else
          format.html { redirect_to action: :index}
        end
        format.html { redirect_to action: :index}        
        format.json { render json: @project, status: :updated, location: @project}
        flash[:notice] = 'Project was updated successfully'
      else
        format.html { render action: :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @project = Project.find(params[:id])
  end

  def destroy
    @project = Project.find(params[:id])
    @project.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
      format.json { head :no_content }
      flash[:notice] = "Project was deleted"
    end
  end
  
  def make_live
    @project = Project.find(params[:id])
    respond_to do |format|
      @project.update_attributes(live_date:DateTime.now, active:true)
      format.html { redirect_to edit_individual_project_path(@project)}
      format.json { render json: @project, status: :updated, location: @project}
      flash[:notice] = 'Project was maked to live state'
    end
  end  
  
  def upload_image
    content_type = params[:file].content_type.split("/").first
    #geometry = MiniMagick::Image.open image.img_url
    if content_type == "image"
      image    = current_user.images.create params.permit(:file, :alt, :hint)
      render json: {
        image: {
          url:    image.img_url,
          # height: 100,
          # width:  100
        }
      }, layout: false, content_type: "text/html"
    else
      render json: {
        error: {
        message: "Invalid file type. Only .jpg, .png and .gif allowed"
        }
      }, layout: false, content_type: 'text/html'
    end
  end

end
