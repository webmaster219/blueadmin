class Task
  include Mongoid::Document
  include Mongoid::Timestamps

	# mount_uploader :attach, TaskAttachUploader

  field :name,								type: String  
  field :duration,         		type: String
  field :start_date,          type: Date
  field :end_date,            type: Date  
  field :description,         type: String  
  field :attach,					    type: String

  field :completed,           type: Boolean,      default: false
  field :owners_name,         type: String
  
  field :attach_file_ids,     type: String

  belongs_to :owner
  belongs_to :project
  validates_presence_of :project_id, :name, :start_date, :end_date
  
  validate :end_date_validate

  def user
    project.present? ? project.user : nil
  end
  def attach_files
    if attach_file_ids.present?
      ids = attach_file_ids.split(",")
      AttachFile.in(id:ids)
    else
      []
    end
  end
  def owner_name
    if owner.present?
      owner.name
    else
      owners_name
    end
  end

  def attach_url
    host = ""
    if attach.url.nil?
      "none_image"
    else
      if Rails.env.production?
        attach.url
      else
        attach.url.gsub("#{Rails.root.to_s}/public/", "/")
      end
    end
  end

  private  
  def end_date_validate
    if start_date > end_date
      errors.add(:end_date, "wrong, end_date should not be large than start_date ")
    end
  end

end
