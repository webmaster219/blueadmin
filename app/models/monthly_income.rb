class MonthlyIncome
  include Mongoid::Document
  include Mongoid::Timestamps

  # mount_uploader :attach, TaskAttachUploader
  field :year,                 type: Integer
  field :month,                 type: Integer  
  field :incomes,               type: Integer
    
  belongs_to :project

  validates_presence_of :project_id, :year, :month, :incomes
  
  def self.incomes_of_month year, month
    incomes = where(year:year, month:month)
    incomes.map(&:incomes).sum
  end

  def self.incomes_of_year
    self.map(&:incomes).sum
  end
end
