class UsersController < ApplicationController    
  before_filter :authenticate_user!, except: [:pricing, :purchase] #, :only => [:setting, :update, ]
  skip_before_filter :verify_authenticity_token, :only => [:add_card, :purchase]
  @@process = -1  
  def index    
    #@users = User.all
  end

  def update
    user = current_user
    data = ''
    case params[:state]
    when '0'
      if user.update_attributes(first_name:params[:user][:first_name], last_name:params[:user][:last_name], email:params[:user][:email])
        data = {:success => 'Updated your basic information successfully'}
      else
        data = {:failure => user.errors.messages}
      end
    when '1'
      if user.valid_password?(params[:user][:cur_password])
        if user.update_attributes(password:params[:user][:password], password_confirmation:params[:user][:password_confirmation])
          data = {:success => 'Changed your password'}
        else
          data = {:failure => user.errors.messages}
        end
      else
        data = {:failure => "Please check your current password"}
      end
    when '2'
      st_dt = params[:user][:start_financial_year].present? ? Date.strptime(params[:user][:start_financial_year], "%d/%m/%Y") : nil
      ed_dt = params[:user][:end_financial_year].present? ? Date.strptime(params[:user][:end_financial_year], "%d/%m/%Y") : nil
      
      if user.update_attributes(start_financial_year:st_dt, end_financial_year:ed_dt, default_currency:params[:user][:default_currency], income_target:params[:user][:income_target], year_budget:params[:user][:year_budget], send_me_alert:params[:user][:send_me_alert])
        data = {:success => 'Updated your other informations successfully'}
      else
        data = {:failure => user.errors.messages}
      end
    end
    render json:data and return
  end

  def setting
    @user = current_user
    @uploaded_file = current_user.uploaded_file_size
    @upload_state = (@uploaded_file/current_user.file_limit)*100    
  end

  def add_card    
    user = current_user    
    token = params[:stripeToken]
    if token.present?
      if user.payment_profile.present?
        p_profile = user.payment_profile
        p_profile.update_attributes(first_name: params[:card_user_name], exp_month: params[:exp_month], exp_year: params[:exp_year], ccard_last4: params[:cvc_number])
      else        
        p_profile = user.build_payment_profile(first_name: params[:card_user_name], exp_month: params[:exp_month], exp_year: params[:exp_year], ccard_last4: params[:cvc_number])
        p_profile.save
        customer = Stripe::Customer.create(
          :card => token,
          :plan => "large",
          :email => user.email,
          :description=> user.name
        )
      end
    end
    user.update_attributes(customer_id:customer.id)
    redirect_to action: :setting
  end

  def destroy
    @user = current_user
    @user.destroy
    
    cu = Stripe::Customer.retrieve(@user.customer_id)
    cu.delete
    sign_out
    flash[:notice] = "We're sorry to see you go."
    #flash[:notice] = "We're sorry to see you go. Send us some feedback before you go!"    
    redirect_to root_path    
  end


  def pricing
    @user = User.find(params[:id])
    @member_price = @user.member_price
    #User::MEMBER_TYPES[@user.member_type]
  end

  def purchase
    @user = User.find(params[:user_id])
    token = params[:stripeToken]
    begin
      customer = Stripe::Customer.create(
        :card => token,
        :plan => User::MEMBER_TYPES[@user.member_type],
        :email => @user.email,
        :description=> @user.name
      )
      @user.update_attributes(customer_id:customer.id)

      if @user.payment_profile.present?
        p_profile = @user.payment_profile
        p_profile.update_attributes(exp_month: params[:exp_month], exp_year: params[:exp_year], ccard_last4: params[:cvc_number])        
      else
        p_profile = @user.build_payment_profile(exp_month: params[:exp_month], exp_year: params[:exp_year], ccard_last4: params[:cvc_number])
        p_profile.save
      end

      sign_in(:user, @user)
      redirect_to user_thanks_path and return

    rescue Stripe::CardError => e
      redirect_to user_pricing_path(@user.id)
      flash[:error] = "Oh snap! Your payment failed please try again or contact your baknk"
    end
    # customer = Stripe::Customer.create(
    #   :card => token,
    #   :plan => "large",
    #   :email => @user.email
    # )
  end

  def thanks    
  end

  def money_converting
    @@process += 1
    render json: {success: @@process}
  end
end
