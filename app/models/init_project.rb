class InitProject
  include Mongoid::Document
  include Mongoid::Timestamps

  IMPORT_TABLES=%w[accounts articles project keywordarticles tasks order&income expenses]

  attr_accessor :csv_content

  field :uploaded_date,     type: Date, default: Time.now  
  field :table_name,        type: String
  field :content,           type: String, default: ""

  belongs_to :user

  
  #validates_uniqueness_of :site_code
  
end
