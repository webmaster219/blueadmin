class Keyword
  include Mongoid::Document
  include Mongoid::Timestamps

  field :pri_key,             type: String
  field :sec_key,             type: String

  field :pri_key_difficult,   type: String
  field :sec_key_difficult,   type: String

  field :pri_key_monthly,     type: Integer
  field :sec_key_monthly,     type: Integer

  belongs_to :project
end
