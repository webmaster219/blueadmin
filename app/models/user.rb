class User
  include Mongoid::Document
  include Mongoid::Timestamps
  
  SEND_ME_ALERT_TYPES = %w[Every\ Month Every\ Week]
  MEMBER_TYPES = %w[large medium most person]
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :email,                   type: String, default: ""
  field :encrypted_password,      type: String, default: ""

  ## Recoverable
  field :reset_password_token,    type: String
  field :reset_password_sent_at,  type: Time

  ## Rememberable
  field :remember_created_at,     type: Time

  ## Trackable
  field :sign_in_count,           type: Integer, default: 0
  field :current_sign_in_at,      type: Time
  field :last_sign_in_at,         type: Time
  field :current_sign_in_ip,      type: String
  field :last_sign_in_ip,         type: String

  # Confirmable
  field :confirmation_token,   type: String
  field :confirmed_at,         type: Time
  field :confirmation_sent_at, type: Time
  field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time

  field :first_name,              type: String, default: ""
  field :last_name,               type: String, default: ""
  field :url,                     type: String, default: ""
  
  field :start_financial_year,    type: Date
  field :end_financial_year,      type: Date

  field :default_currency,        type: Integer

  field :income_target,           type: Integer, default: 0
  field :year_budget,             type: Integer, default: 0
  field :send_me_alert,           type: Integer, default: 0

  field :member_type,             type: Integer, default: 0
  field :customer_id,             type: String
  field :role,                    type: String
  field :status,                  type: String, default: 'Live'
  field :authentication_token,    type: String
  
  before_save :ensure_authentication_token
  

  has_one :payment_profile,       dependent: :destroy

  has_many :options
  has_many :projects 
  has_many :init_projects,        dependent: :destroy
  has_many :owners,               dependent: :destroy

  has_many :images
  

  #validates_presence_of :first_name, :last_name, :email
  def currency_type
    if default_currency.nil?
      Payment::CURRENCY_TYPES[1]
    else
      Payment::CURRENCY_TYPES[default_currency]
    end
  end

  def currency_symbol
    money = Money.new(1,currency_type)
    money.currency.symbol
  end

  def expenses
    project_ids = projects.map(&:id)
    Expense.in(project_id:project_ids)
  end

  def tasks
    project_ids = projects.map(&:id)
    Task.in(project_id:project_ids)
  end

  def articles
    project_ids = projects.map(&:id)
    Article.in(project_id:project_ids)
  end

  def keywords
    project_ids = projects.map(&:id)
    Keyword.in(project_id:project_ids)
  end

  def monthly_orders
    project_ids = projects.map(&:id)
    MonthlyOrder.in(project_id:project_ids)
  end
  def monthly_incomes
    project_ids = projects.map(&:id)
    MonthlyIncome.in(project_id:project_ids)
  end

  def monthly_incomes_sum
    monthly_incomes.map(&:incomes).sum
  end

  def future_projects
    projects.future_projects
  end
  
  def live_projects    
    projects.live_projects
  end

  def name
    [first_name, last_name].join(" ")
  end

  def veryfied?
    payment_profile.present?
  end

  def file_limit
    case member_type
    when 0
      10
    when 1
      250
    when 2
      1024
    when 3
      2048
    end
  end

  def member_price
    case member_type
    when 0
      19.99  
    when 1
      29.99
    when 2
      49.99
    when 3
      79.99
    end
  end
  def last_payment
    if customer_id.present?
      begin
        customer = Stripe::Customer.retrieve(customer_id)
        date = customer.invoices.data.last[:date]    
        #if customer.invoices.data.last[:paid] == true
        Time.at(date.to_i).to_datetime.strftime("%d/%m/%Y") 
      rescue Stripe::CardError => e
        'CardError'
      end
    end
  end
  def update_plan(role)
    unless customer_id.nil?
      customer = Stripe::Customer.retrieve(customer_id)
      customer.update_subscription(:plan => role)
    end
    true
  rescue Stripe::StripeError => e
    logger.error "Stripe Error: " + e.message
    errors.add :base, "Unable to update your subscription. #{e.message}."
    false
  end

  def self.find_by_customer_id(id)
    User.where(customer_id:id).first
  end

  def cancel_subscription
    unless customer_id.nil?
      customer = Stripe::Customer.retrieve(customer_id)
      unless customer.nil? or customer.respond_to?('deleted')
        subscription = customer.subscriptions.data[0]
        if subscription.status == 'active'
          customer.cancel_subscription
        end
      end
    end
  rescue Stripe::StripeError => e
    logger.error "Stripe Error: " + e.message
    errors.add :base, "Unable to cancel your subscription. #{e.message}."
    false
  end

  def expire
    UserMailer.expire_email(self).deliver
    self.update_attribute(:status, 'expired')
  end

  def ensure_authentication_token
    self.authentication_token ||= generate_authentication_token
  end

  def is_admin?
    role == 'admin'
  end

  def uploaded_file_size
    # return 11
    size = 0
    s3=AWS::S3.new(:access_key_id=>"AKIAJHWG3GA6TREPNP6A", :secret_access_key=>'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')
    bucket = s3.buckets['adminapp']
    my_objects = bucket.objects.with_prefix("public/#{self.id.to_s}/")
    my_objects.each do |obj|
      size += obj.content_length
    end
    (size / 1024.0 / 1024.0).round(2)
  end


  def self.send_email_alert
    cur_date = Time.now.to_date
    User.all.each do |user|
      count = (cur_date - user.created_at.to_date).to_i
      if user.send_me_alert == 0   # send me alert per monthly
        if count % 30 == 0
          UserMailer.send_alert(user).deliver 
        end
      elsif user.send_me_alert ==1 # send me alert per weekly
        if count % 7 == 0
          UserMailer.send_alert(user).deliver 
        end
      end
    end
  end

  def self.send_test_email
    user = User.where(email:'khayamu@gmail.com').first
    # user = User.last
    UserMailer.send_alert(user).deliver
  end

  private
  def generate_authentication_token
    loop do 
      token = Devise.friendly_token
      break token unless User.where(authentication_token:token).first
    end
  end  
end
