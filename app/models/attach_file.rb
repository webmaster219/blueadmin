class AttachFile
  include Mongoid::Document
  include Mongoid::Timestamps
  mount_uploader :file, AttachFileUploader

  field :file,                  type: String

  belongs_to :attachable,       polymorphic: true
  belongs_to :user
  belongs_to :project
  belongs_to :task
  
  validates_presence_of :user_id, :file

  def file_size
    begin 
      (file.size / 1024.0).round(2)
    rescue
      0
    end
  end
  def to_jq_upload
    {
      "id" => id.to_s,
      "name" => read_attribute(:file),
      "size" => file.size,
      "url" => file.url,
      "delete_url" => "/attach_files/#{id}",
      "delete_type" => "DELETE"
    }
  end

  def attach_url
    if self.file.url.nil?
      ""
    else
      # if Rails.env.production?
        self.file.url
      # else
      #   "http://localhost:3000" + self.file.url.gsub("#{Rails.root.to_s}/public/", "/")        
      # end
    end
  end
end
